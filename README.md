# Deploy to AWS Elastic Beanstalk
An example script for deploying a new version of an application to an existing [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) application with Bitbucket Pipelines.  A sample Python application is included to use as a demo for trying out the code and configuration.

## How To Use It
* Optional:  Create a new [AWS Elastic Beanstalk application](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/GettingStarted.html) using the Python sample application from the drop down box in the console.
* Add the required Environment Variables below.
* Copy `beanstalk_deploy.py` to your project.
* Copy `bitbucket-pipelines.yml` to your project.
    * Or use your own, just be sure to include all steps in the sample yml file.
* Copy the `sample_application` folder to your project if you would like to try this using a sample application.
    * The included sample application is the same as Elastic Beanstalk sample that gets deployed except it includes modified text showing a successful deployment from Bitbucket Pipelines.

## Required Permissions in AWS
It is recommended you [create](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) a separate user account used for this deploy process.  This user should be associated with a group that has the `AWSElasticBeanstalkFullAccess` [AWS managed policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html) attached for the required permissions to execute a new deployment to AWS Elastic Beanstalk.

Note that the above permissions are more than what is required in a real scenario. For any real use, you should limit the access to just the AWS resources in your context.

## Required Environment Variables
* `AWS_SECRET_ACCESS_KEY`:  Secret key for a user with the required permissions.
* `AWS_ACCESS_KEY_ID`:  Access key for a user with the required permissions.
* `AWS_DEFAULT_REGION`:  Region where the target Elastic Beanstalk application is.
* `APPLICATION_NAME`:  Name of the target Elastic Beanstalk application.
* `APPLICATION_ENVIRONMENT`:  Name of the Elastic Beanstalk environment you are deploying to.
* `S3_BUCKET`:  Name of the S3 Bucket where application versions for the Elastic Beanstalk application are stored.
    * AWS Elastic Beanstalk will create a bucket in the region automatically with a name like `elasticbeanstalk-us-west-2-YOUR_ACCOUNT_NUMBER`.  You can use this bucket to upload your application source.

# License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.
